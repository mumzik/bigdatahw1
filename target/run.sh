#!/usr/bin/env bash

dir=$(( RANDOM ))

echo "##JOB STARTED"

yarn jar ./sashin-hw1-1.0.jar /user/hduser/input1 /user/hduser/input2 /user/hduser/output/$dir

echo "##JOB FINISHED"

hadoop fs -text /user/hduser/output/$dir/part-r-00000 > /tmp/output.txt
hadoop fs -text /user/hduser/output/$dir/part-r-00001 >> /tmp/output.txt
hadoop fs -text /user/hduser/output/$dir/part-r-00002 >> /tmp/output.txt
hadoop fs -text /user/hduser/output/$dir/part-r-00003 >> /tmp/output.txt

echo "## RESULT"

cat /tmp/output.txt
