#!/usr/bin/env bash

hdfs dfs -mkdir /user/hduser
hdfs dfs -mkdir -p /user/hduser/input1
hdfs dfs -mkdir -p /user/hduser/input2
hdfs dfs -mkdir -p /user/hduser/output

hdfs dfs -rm /user/hduser/input1/*
hdfs dfs -rm /user/hduser/input2/*

hdfs dfs -put ./input1.txt /user/hduser/input1
hdfs dfs -put ./input2.txt /user/hduser/input2
