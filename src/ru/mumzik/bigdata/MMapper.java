package ru.mumzik.bigdata;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MMapper extends Mapper<Object, Text, Text, IntWritable> {

    private IntWritable count=new IntWritable();
    private Text clientAgent=new Text();
    private Pattern pattern=Pattern.compile("^([\\d.]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]+)\" \"(?<clientAgent>[^ ]+)([^\"]+)\" \"([^\"]+)\"");

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String str=value.toString();
        Matcher matcher=pattern.matcher(str);
        if (!matcher.matches()){
           return;
        }
        clientAgent.set(matcher.group("clientAgent"));
        count.set(1);
        context.write(clientAgent,count);
    }
}
