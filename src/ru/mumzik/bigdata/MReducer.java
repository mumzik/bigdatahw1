package ru.mumzik.bigdata;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MReducer extends Reducer<Text, IntWritable,Text, Text> {
    private Text res=new Text();
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int count=0;
        for (IntWritable i:values){
            count+=i.get();
        }

        res.set("total count:"+count);
        context.write(key,res);
    }
}
