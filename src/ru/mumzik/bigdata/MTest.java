package ru.mumzik.bigdata;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MTest {


    private static final int COUNT = 42;
    private static final String KEY = "Mozila/5.0";
    MapDriver<Object, Text, Text, IntWritable> mapperDriver;
    ReduceDriver<Text, IntWritable, Text, Text> reduceDriver;

    @Before
    public void setUp() {
        MMapper mapper = new MMapper();
        MReducer reducer = new MReducer();
        mapperDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapperDriver.withInput(new Object(), new Text("109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] \"GET /administrator/ HTTP/1.1\" 200 4263 \"-\" \"Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0\" \"-\""));
        mapperDriver.withInput(new Object(), new Text("131.108.164.124 - - [16/Dec/2015:09:30:30 +0100] \"GET /wp-login.php HTTP/1.1\" 404 218 \"http://www.almhuette-raith.at/\" \"Opera/9.80 (Windows NT 6.2; Win64; x64) Presto/2.12.388 Version/12.17\" \"-\""));
        mapperDriver.withOutput(new Text("Mozilla/5.0"), new IntWritable(1));
        mapperDriver.withOutput(new Text("Opera/9.80"),  new IntWritable(1));

        mapperDriver.runTest();
    }


    @Test
    public void testReducer() throws IOException {
        List<IntWritable> values1 = new ArrayList<IntWritable>();
        for (int i = 0; i< COUNT; i++)
        values1.add(new IntWritable(1));
        reduceDriver.withInput(new Text(KEY), values1);


        reduceDriver.withOutput(new Text(KEY), new Text(String.valueOf(COUNT)));

        reduceDriver.runTest();
    }

}
