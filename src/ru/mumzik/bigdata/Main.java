package ru.mumzik.bigdata;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class Main extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        Configuration conf=new Configuration();
        int res=ToolRunner.run(conf,new Main(),args);
        System.exit(res);

    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf=new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator", ";");

        Job job = Job.getInstance(conf, "MapReduce JOB");

        job.setJarByClass(Main.class);
        job.setReducerClass(MReducer.class);

        //Map
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //Reduce
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setNumReduceTasks(4);

        //Input files
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, MMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MMapper.class);

        //Output file
        FileOutputFormat.setOutputPath(job,  new Path(args[2]));
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        FileOutputFormat.setCompressOutput(job,true);
        FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
